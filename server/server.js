var net = require('net');

var headerSize = 4 + 1;

var CMD_HELLO = 1;
var CMD_READY = 2;
var CMD_NEW_PLAYER = 3;
var CMD_START = 4;
var CMD_TURN = 5;
var CMD_END = 6;

var OK = 200;

var currentTurn = 0;
var room = [];


function User(c) {
	this.c = c;
}

User.prototype.quit = function() {
	var user = this;
	room = room.filter(function (u) {return u !== user});
	if (room.length == 0) {
		currentTurn = 0;
	}
};

User.prototype.sendAll = function(type, content) {
	for (var i in room) {
		room[i].sendMsg(type, content);
	}
}

User.prototype.sendMsg = function(type, content) {
	var buffer = content.getBuffer();

	var header = new BufferWriter();

	header.writeUInt32(content.getLength());
	header.writeUInt8(type);
	
	console.log('writing', header.getBuffer().length + content.getBuffer().length);

	this.c.write(Buffer.concat([header.getBuffer(), content.getBuffer()]));
};

User.prototype.processMsg = function(buffer) {
	var msg = new BufferReader(buffer);

	var length = msg.readUInt32();
	var type = msg.readUInt8();

	switch (type) {
		case CMD_END:
			this.quit();
			this.c.end();
			break;
		case CMD_HELLO:
			var callbackId = msg.readUInt32();

			this.login = msg.readString();
			

			room.push(this);
			this.id = room.length;


			var writer = new BufferWriter();
			
			writer.writeUInt32(callbackId);

			writer.writeUInt8(room.length);
			for (var i in room) {
				writer.writeUser(room[i]);
			}

			this.sendMsg(type, writer);


			var newPlayer = new BufferWriter();
			newPlayer.writeUser(this);
			
			this.sendAll(CMD_NEW_PLAYER, newPlayer);


			break;
		case CMD_READY:

			this.ready = true;
			var ready = new BufferWriter();
			ready.writeUserId(this);

			this.sendAll(CMD_READY, ready);
			
			var allReady = true;
			for (var i in room) {
				if (!room[i].ready) {
					allReady = false;
				}
			}
			
			if (allReady) {
				var start = new BufferWriter();
				start.writeUInt8(room.length);

				for (var i in room) {
					start.writeUser(room[i]);
				}
			
				this.sendAll(CMD_START, start);
			}

			break;
		case CMD_READY:
			break;

		case CMD_TURN:
			var turn = msg.readUInt32();


			this.turnReady = turn;
			this.cmd = msg.buffer.slice(msg.p);

			var allReady = true;
			for (var i in room) {
				if (room[i].turnReady != currentTurn+1) {
					allReady = false;
				}
			}

			if (allReady) {
				var turn = new BufferWriter();

				turn.writeUInt8(room.length);


				for (var i in room) {
					turn.writeUserId(room[i]);
					turn.writeBuffer(room[i].cmd);
				}


				currentTurn++;
				this.sendAll(CMD_TURN, turn);
			}
			break;
	}
};


function BufferWriter() {
	this.buffer = new Buffer(128);
	this.p = 0;
}

BufferWriter.prototype.extend = function(amount) {
	while (this.p + amount >= this.buffer.length) {
		var old = this.buffer;
		this.buffer = new Buffer(this.buffer.length * 2);
		old.copy(this.buffer);
	}
};

BufferWriter.prototype.writeBuffer = function(sourceBuffer) {
	this.extend(sourceBuffer.length);	
	sourceBuffer.copy(this.buffer, this.p);
	this.p += sourceBuffer.length;
}

BufferWriter.prototype.writeUser = function(user) {
	this.writeString(user.login);
	this.writeUInt8(user.id);
}

BufferWriter.prototype.writeUserId = function(user) {
	this.writeUInt8(user.id);
}

BufferWriter.prototype.writeUInt8 = function(n) {
	this.extend(1);
	this.buffer.writeUInt8(n, this.p);
	this.p++;
};

BufferWriter.prototype.writeUInt32 = function(n) {
	this.extend(4);
	this.buffer.writeUInt32LE(n, this.p);
	this.p += 4;
};

BufferWriter.prototype.writeLBE128 = function(n) {
	if (!n) {
		this.writeUInt8(0);
	}
	while (n) {
		this.writeUInt8((n & 127) | (n > 127 ? 128 : 0));
		n = n >> 7;
	}
};

BufferWriter.prototype.writeString = function(str) {
	var encoded = new Buffer(str, 'utf8');
	this.writeLBE128(encoded.length);
	this.extend(encoded.length);
	encoded.copy(this.buffer, this.p);
	this.p += encoded.length;
};

BufferWriter.prototype.getBuffer = function() {
	return this.buffer.slice(this.buffer, this.p);
	
};

BufferWriter.prototype.getLength = function() {
	return this.p;
};


function BufferReader(b) {
	this.buffer = b;
	this.p = 0;
}

BufferReader.prototype.readInt32 = function() {
	var r = this.buffer.readInt32LE(this.p);
	this.p += 4;
	return r;
};

BufferReader.prototype.readUInt8 = function() {
	var r = this.buffer.readUInt8(this.p);
	this.p += 1;
	return r;
};

BufferReader.prototype.readUInt32 = function() {
	var r = this.buffer.readUInt32LE(this.p);
	this.p += 4;
	return r;
};

BufferReader.prototype.readFloat = function() {
	var r = this.buffer.readFloatLE(this.p);
	this.p += 4;
	return r;
};

BufferReader.prototype.readLBE128 = function() {
	var r = 0; 
	var s = 0;
	while (true) {
		var b = this.buffer.readUInt8(this.p);
		this.p += 1;
		r = r | ((b & 127) << s);
		if ((b & 128) == 0) {
			break;

		}
		s += 7;
	}
	return r;
};

BufferReader.prototype.readString = function() {
	var len = this.readLBE128();
	var r = this.buffer.toString("utf8", this.p, this.p + len);
	this.p += len;
	return r;
};

BufferReader.getPosition = function() {
	return this.p;
};


function test(n) {
	var writer = new BufferWriter();
	writer.writeLBE128(n);
	var buf = writer.getBuffer();
	var m = new BufferReader(buf).readLBE128(buf);
	if (m == n) {
//		console.log("ok", n);
	} else {
		console.log("not ok #", n, " != ", m);
	}
}

test(0);
test(100);
test(102302);
test(7);
test(128);
test(200);


var server = net.createServer(function(c) {
	var user = new User(c);
	c.setNoDelay(true);
	c.setKeepAlive(false);
	var buffer = new Buffer(0);
	c.on('data', function(data) {
		console.log('got', data.length);
		buffer = Buffer.concat([buffer, data]);

		while (buffer.length >= headerSize) {
			var msgLength = buffer.readUInt32LE(0);
			var type = buffer.readUInt8(4);

			var fullLength = msgLength + headerSize;

			if (buffer.length < fullLength) {
				return;
			}

			user.processMsg(buffer.slice(0, fullLength));

			buffer = buffer.slice(fullLength);
		}

	});

	c.on('error', function() {
		user.quit();
	});
	c.on('end', function() {
		user.quit();
	});

});

server.listen(4321, function() {
	console.log('server msg');
});
