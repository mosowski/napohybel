﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Server {

	class Connection {
		public Udp.Remote remote;
		public bool hasJoined;
		public byte id;
		public string login;
		public bool isReady;
		public uint currentTurn;
		public List<Command> commands = new List<Command>();
	}

	public float broadcastInterval = 1.0f;

	Udp udp;
	float lastBroadcastTime;
	byte idGen = 0;
	List<Connection> connections = new List<Connection>();
	bool hasMatchStarted = false;

	uint currentTurn = 1;

	public Server() {
		udp = new Udp();
		udp.AllowAddressReuse();
		udp.Listen(_.self.port);
	}

	IEnumerable<Connection> JoinedPlayers {
		get {
			foreach (var c in connections) {
				if (c.hasJoined) {
					yield return c;
				}
			}
		}
	}

	IEnumerable<Connection> ReadyPlayers {
		get {
			foreach (var c in connections) {
				if (c.isReady) {
					yield return c;
				}
			}
		}
	}

	Connection GetConnection(Udp.Remote remote) {
		foreach (var c in connections) {
			if (c.remote == remote) {
				return c;
			}
		}
		{
			var c = new Connection() { remote = remote };
			connections.Add(c);
			return c;
		}
	}

	void SendAllJoined(Action<Udp.MessageBuffer> fill, bool reliable) {
		foreach (var item in JoinedPlayers) {
			Send(item, fill, reliable);
		}
	}

	void Broadcast(Action<Udp.MessageBuffer> fill) {
		var buf = udp.GetBroadcast(_.self.port+1).BeginWriteSimple();
		fill(buf);
		udp.GetBroadcast(_.self.port+1).EndWrite(buf);

	}

	void Send(Connection c, Action<Udp.MessageBuffer> fill, bool reliable) {
		var buf = reliable ? c.remote.BeginWriteComplex() : c.remote.BeginWriteSimple();
		fill(buf);
		c.remote.EndWrite(buf);
	}

	void WritePlayers(Udp.MessageBuffer b) {
		int numJoined = JoinedPlayers.Count();

		b.Write((byte)numJoined);
		foreach (var c in JoinedPlayers) {
			b.Write(c.id);
			b.Write(c.login);
			b.Write(c.isReady);
		}
	}

	void CheckStart() {
		if (ReadyPlayers.Count() == JoinedPlayers.Count()) {
			SendAllJoined((b) => {
				b.Write((byte)MessageType.Start);
				WritePlayers(b);
			}, true);

			hasMatchStarted = true;
		}
	}

	void CheckTurn() {
		int numComplete = 0;

		foreach (var c in connections) {
			numComplete += (c.currentTurn == currentTurn) ? 1 : 0;
		}

		if (numComplete == ReadyPlayers.Count()) {
			SendAllJoined((b) => {
				b.Write((byte)MessageType.Turn);
				b.Write((byte)numComplete);
				foreach (var c in connections) {
					b.Write(c.id);
					b.Write((byte)c.commands.Count);
					for (int i = 0; i < c.commands.Count; ++i) {
						c.commands[i].Serialize(b);
					}
				}
			}, true);

			currentTurn++;
		}
	}

	public void Update() {
		udp.Update();

		foreach (var r in udp.remotes) {
			var c = GetConnection(r);

			while (r.CanRead) {
				var b = r.BeginRead();
				MessageType type = (MessageType)b.ReadByte();

				if (!hasMatchStarted) {
					if (type == MessageType.Join) {
						c.id = idGen++;
						c.login = b.ReadString();
						Debug.Log("<color=green>PLAYER " + c.login + " JOINED</color>");
						c.isReady = false;
						c.hasJoined = true;
					} 

					if (type == MessageType.Ready) {
						c.isReady = true;
					}
				} else {
					if (type == MessageType.Turn) {
						c.currentTurn = currentTurn;
						int numCommands = (int)b.ReadByte();
						c.commands.Clear();
						for (int i = 0; i < numCommands; ++i) {
							byte commandType = b.ReadByte();
							c.commands.Add(Command.constructors[commandType](b));
						}
					}
				}

				if (type == MessageType.End) {
					connections.Remove(c);
				}

				r.EndRead(b);
			}

			if (!hasMatchStarted) {
				CheckStart();
			} else {
				CheckTurn();
			}
		}

		if (_.self.gameState == GameState.Lobby) {
			if (Time.time - lastBroadcastTime > 1.0f) {
				Broadcast((b) => {
					b.Write((byte)MessageType.RoomState);
					WritePlayers(b);
				});
				Debug.Log("Sent broadcast");

				lastBroadcastTime = Time.time;
			}
		}
	}

	public void End() {
		if (_.self.gameState == GameState.Lobby) {
			Broadcast((b) => {
				b.Write((byte)MessageType.End);
			});
		}
	}

}
