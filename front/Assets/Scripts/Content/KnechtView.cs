﻿using UnityEngine;
using System.Collections;

public class Knecht : Unit {

	int lastShootTurn = 0;

	override public void Tick() {
		base.Tick();

		if (!isMoving) {
			if (_.self.CurrentTurn - lastShootTurn > model.autofireInterval) {
				Unit enemy = _.self.GetNearestEnemyUnit(squad.player, position);
				if (enemy != null) {
					Projectile p = _.self.CreateProjectile(model.projectileModel, squad.player, position, enemy.position);
					p.damage = model.projectileModel.damage;
					lastShootTurn = _.self.CurrentTurn;
				}
			}
		} else {
			Move();
		}
	}
}

public class KnechtView : UnitView {

	override public Unit Construct() {
		return new Knecht();
	}

}
