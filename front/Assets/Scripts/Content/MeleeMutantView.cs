﻿class MeleeMutant : Unit {
    int meleeCooldown = 0;
    int meleeDuration = 0;
    Unit meeleTarget;
    bool isAttacking;

    override public void Tick() {
        base.Tick();

        if (isAttacking) {
            if (meleeDuration == 0) {
                if ((meeleTarget.position - position).magnitude < model.meleeExtraRadius) {
                    _.self.DealDamage(squad.player, model.meleeDamage, meeleTarget);
                }
                isAttacking = false;
            } else {
                meleeDuration--;
            }
        } else {
            Move();
            if (meleeCooldown == 0) {
                Unit enemy = _.self.GetNearestEnemyUnit(squad.player, position);
                if (enemy != null) {
                    if ((enemy.position - position).magnitude < model.meleeRadius) {
                        meeleTarget = enemy;
                        isAttacking = true;
                    }
                }
            } else {
                meleeCooldown--;
            }
        }
    }
}

class MeleeMutantView : UnitView {
    override public Unit Construct() {
        return new MeleeMutant();
    }
}

