﻿using UnityEngine;
using System.Collections;

public class Missile : Projectile {

	override public void Tick() {
		base.Tick();

		if (Position.Distance(position, target) <= model.speed) {
			if (position == target) {
				_.self.DealSplashDamage(player, target, model.range, damage);
				_.self.DestroyProjectile(this);
			} else {
				position = target;
			}
		} else {
			position += (target - position).normalized * model.speed;
		}
	}
}

public class MissileView : ProjectileView {

	override public Projectile Construct() {
		return new Missile();
	}

	override public void Init(Projectile p) {
		base.Init(p);

		transform.rotation = Quaternion.FromToRotation(new Vector3(1, 0, 0), (p.target - p.origin).normalized);
	}

}
