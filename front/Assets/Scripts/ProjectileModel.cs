﻿using UnityEngine;
using System.Collections;

public class ProjectileModel : MonoBehaviour {

	public enum ProjectileType {
		Missile,
		Bullet,	
		Ray
	}

	public ProjectileType type;
	public GameObject prefab;
	public float speed;
	public float range;
	public int damage;
}
