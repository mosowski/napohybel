﻿using UnityEngine;
using System.Collections;

public class ProjectileView : MonoBehaviour {
	public Projectile projectile;
	Position oldPosition;
	Position newPosition;
	float lastTickTime;

	public ProjectileModel Model {
		get {
			return projectile.model;
		}
	}

	public virtual Projectile Construct() {
		return null;
	}

	public virtual void Init(Projectile p) {
		projectile = p;
		transform.position = p.position;
	}

	public virtual void Tick() {
		oldPosition = transform.position;
		newPosition = projectile.position;
		lastTickTime = Time.time;
	}

	public virtual void Update() {
		float d = (Time.time - lastTickTime) / _.self.turnDuration;
		d = Mathf.Clamp01(d);
		transform.position = oldPosition + (newPosition - oldPosition) * d;
	}

	public virtual void Die() {
		_.self.pool.Release(gameObject);
	}
}
