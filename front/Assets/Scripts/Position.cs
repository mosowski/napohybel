﻿using UnityEngine;
using System.Collections;

public struct Position {
	public float x;
	public float y;

	public Position(float _x = 0, float _y = 0) {
		x = _x;
		y = _y;
	}

	static public bool operator ==(Position a, Position b) {
		return a.x == b.x && a.y == b.y;
	}

	static public bool operator !=(Position a, Position b) {
		return a.x != b.x || a.y != b.y;
	}

	static public Position operator -(Position a, Position b) {
		return new Position(a.x - b.x, a.y - b.y);
	}

	static public Position operator +(Position a, Position b) {
		return new Position(a.x + b.x, a.y + b.y);
	}

	static public Position operator *(Position a, float b) {
		return new Position(a.x * b, a.y * b);
	}

	static public implicit operator Vector3(Position p) {
		return new Vector3(p.x, 0, p.y);
	}
	
	static public implicit operator Vector2(Position p) {
		return new Vector2(p.x, p.y);
	}

	static public implicit operator Position(Vector3 v) {
		return new Position(v.x, v.z);
	}

	static public implicit operator Position(Vector2 v) {
		return new Position(v.x, v.y);
	}

	static public float Distance(Position a, Position b) {
		return Vector2.Distance(a, b);
	}

	public float magnitude {
		get {
			return Mathf.Sqrt(x * x + y * y);
		}
	}

	public void Normalize() {
		float m = magnitude;
		if (m != 0) {
			x /= m;
			y /= m;
		}
	}

	public Position normalized {
		get {
			Position b = new Position(x, y);
			b.Normalize();
			return b;
		}
	}
}
