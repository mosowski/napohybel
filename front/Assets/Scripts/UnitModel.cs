﻿using UnityEngine;
using System.Collections;

public class UnitModel : MonoBehaviour {
	public GameObject prefab;
	public int hp;
	public float speed;
	public float radius;

    public float meleeRadius;
    public int meleeDamage;
    public float meleeExtraRadius;

	public ProjectileModel projectileModel;
	public int autofireInterval;
}
