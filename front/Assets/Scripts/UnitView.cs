﻿using UnityEngine;
using System.Collections;

public class UnitView : MonoBehaviour {
	public Unit unit;
	Position oldPosition;
	Position newPosition;
	float lastTickTime;

	GameObject moveMarker;

	public UnitModel Model {
		get {
			return unit.model;
		}
	}

	public virtual Unit Construct() {
		return null;
	}

	public virtual void Init(Unit u) {
		unit = u;
		transform.position = u.position;
	}

	public virtual void StartMoving() {
		if (unit.player == _.self.owner) {
			if (moveMarker == null) {
				moveMarker = _.self.pool.Pick(_.self.moveMarkerPrefab);
			}
			moveMarker.transform.position = unit.moveTarget;
		}
	}

	public virtual void StopMoving() {
		_.self.pool.Release(moveMarker);
		moveMarker = null;
	}

	public virtual void UpdateImmediately() {
		transform.position = unit.position;
		oldPosition = unit.position;
		newPosition = unit.position;
	}

	public virtual void Tick() {
		oldPosition = transform.position;
		newPosition = unit.position;
		lastTickTime = Time.time;
	}

	public virtual void Die() {
		_.self.pool.Release(gameObject);
		if (moveMarker != null) {
			_.self.pool.Release(moveMarker);
		}
	}

	public virtual void Update() {
		float d = (Time.time - lastTickTime) / _.self.turnDuration;
		d = Mathf.Clamp01(d);
		transform.position = oldPosition + (newPosition - oldPosition) * d;
	}
}
