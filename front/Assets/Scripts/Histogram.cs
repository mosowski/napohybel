﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Histogram : MonoBehaviour {

	public GameObject baseSlot;
	public List<float> thresholds = new List<float>();

	List<GameObject> bucketSlots = new List<GameObject>();
	List<int> counts = new List<int>();

	
	public void Awake() {
		int numBuckets = thresholds.Count + 1;

		bucketSlots.Add(baseSlot);
		counts.Add(0);
		for (int i = 1; i < numBuckets; ++i){
			GameObject s = Instantiate(baseSlot);
			bucketSlots.Add(s);
		}

		for (int i = 0; i < numBuckets; ++i){
			GameObject s = bucketSlots[i];
			s.GetComponent<Chui.AnchoredWidget>().leftAnchor.relative = (float)(i) / (numBuckets);
			s.GetComponent<Chui.AnchoredWidget>().rightAnchor.relative = (float)(i + 1) / (numBuckets);
			if ( i < thresholds.Count) {
				Chui.Util.GetChildComponent<Chui.TextContent>(s, "Label").text = thresholds[i].ToString();
			} else {
				Chui.Util.GetChildComponent<Chui.TextContent>(s, "Label").text = "inf";
			}
			s.transform.SetParent(transform);
			counts.Add(0);
		}
	}

	public void Add(float value) {
		for (int i = 0; i < thresholds.Count; ++i) {
			if (value < thresholds[i]) {
				Add(i);
				return;
			}
		}
		Add(thresholds.Count);
	}

	public void Add(int bucket) {
		counts[bucket]++;
	}

	void Update() {
		int max = 0;
		foreach (var c in counts) {
			max = c > max ? c : max;
		}

		for (int i = 0; i < bucketSlots.Count; ++i) {
			float w = 0;
			if (max > 0) {
				w = (float)(counts[i])/max;
			}
			GameObject s = bucketSlots[i];
			s.transform.Find("Value").GetComponent<Chui.AnchoredWidget>().topAnchor.relative = w;
			Chui.Util.GetChildComponent<Chui.TextContent>(s, "Count").text = counts[i].ToString();
		}
	}
}
