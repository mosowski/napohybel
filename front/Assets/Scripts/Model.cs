﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Model : MonoBehaviour {

	public List<UnitModel> units;
	public List<ProjectileModel> projectiles;

	public UnitModel GetUnitModel(string id) {
		foreach (var m in units) {
			if (m.name == id) {
				return m;
			}
		}
		return null;
	}

	public ProjectileModel GetProjectileModel(string id) {
		foreach (var m in projectiles) {
			if (m.name == id) {
				return m;
			}
		}
		return null;
	}
}
