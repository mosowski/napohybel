﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public enum GameState {
	Login,
	Lobby,
	Battle,
	Summary
}

public enum MessageType : byte {
	Join = 1,
	Ready = 2,
	RoomState = 3,
	Start = 4,
	Turn = 5,
	End = 6,

	Ok = 200
}

public class Util {
	static public float Smoothstep(float a, float b, float x) {
		return a + (b - a) * x*x*(3 - 2*x);
	}
}

public class Player {
	public string login;
	public byte id;
	public bool isReady;
	public Squad selectedSquad;

	public List<Squad> squads = new List<Squad>();

	public void AddSquad(Squad s) {
		s.player = this;
		squads.Add(s);
	}

}

public class Squad {
	public Player player;
	public List<Position> points = new List<Position>();
	public List<Unit> units = new List<Unit>();

	public void Tick() {
	}

	public void AddUnit(Unit u) {
		u.squad = this;
		units.Add(u);
	}

	public void RemoveUnit(Unit u) { 
		units.Remove(u);
	}
    
    public bool ShouldReversePoints() {
		if (points.Count != units.Count) {
			Debug.LogWarning("P " + points.Count  + " U "  + units.Count);
		}

        float cost = 0f;
        for (int i = 0; i < points.Count; i++) {
            cost += (points[i] - units[i].position).magnitude;
        }

        float reversedCost = 0f;
        for (int i = 0; i < points.Count; i++) {
            reversedCost += (points[points.Count - 1 - i] - units[i].position).magnitude;
        }

        return reversedCost < cost;

    }

	public void SetLine(List<Position> p) {


		float len = 0;
		for (int i = 0; i < p.Count - 1; ++i) {
			len += Position.Distance(p[i], p[i + 1]);
		}
		points.Clear();
		if (Mathf.Abs(len) < Mathf.Epsilon) {
			for (int i = 0; i < units.Count; ++i) {
				points.Add(p[0]);
			}
			return;
		}

		float space;
		if (units.Count == 0) {
			return;
		} else if (units.Count == 1) {
			space = len / 2;
		} else if (units.Count == 2) {
			points.Add(p[0]);
			points.Add(p[p.Count - 1]);
			return;
		} else {
			space = len / (units.Count - 1);
			points.Add(p[0]);
		}

		float d = 0;
		for (int i = 0; i < p.Count - 1 && points.Count < units.Count - 1; ++i) {
			Position dir = (p[i + 1] - p[i]).normalized;
			Position o = p[i];
			while (true) {
				float segLen = Position.Distance(o, p[i + 1]);
				if (segLen >= space - d) {
					o += dir * (space - d);
					d = 0;
					points.Add(o);
				} else {
					d += segLen;
					break;
				}
			}
		}

		if (points.Count < units.Count) {
			points.Add(p[p.Count - 1]);
		}

        if (ShouldReversePoints()) {
            points.Reverse();
        }
	}

	public void OrderMove() {
		for (int i = 0; i < units.Count; ++i) {
			units[i].OrderMove(points[i]);
		}
	}

	public void ArrangeUnits() {
		for (int i = 0; i < units.Count; ++i) {
			units[i].position = points[i];
			units[i].view.UpdateImmediately();
		}
	}

	public void OrderPrimaryAction(Position position) {
		//TODO
	}
}

public class Unit {
	public bool isDestroyed;

	public UnitView view;
	public UnitModel model;

	public Squad squad;
	public Position position;
	public int hp;

	public bool isMoving;
	public Position moveTarget;


	public Player player {
		get {
			return squad.player;
		}
	}

	public virtual void OrderMove(Position target) {
		moveTarget = target;
		isMoving = true;
		view.StartMoving();
	}

	public virtual void Tick() {
		view.Tick();
    }

    public virtual void Move() {
        if (isMoving) {
            if (Position.Distance(position, moveTarget) > model.speed) {
                position += (moveTarget - position).normalized * model.speed;
            } else {
                position = moveTarget;
                isMoving = false;
				view.StopMoving();
            }
        }
    }


	public virtual void Die() {
		hp = 0;
		view.Die();
		squad.RemoveUnit(this);
	}

	public virtual bool CanPerformPrimaryAction() {
		return false;
	}

	public virtual int PrimaryActionPriority {
		get {
			return -1;
		}
	}

	public virtual bool IsSinglePrimaryAction {
		get {
			return true;
		}
	}

	public virtual void PerformPrimaryAction() {
	}
}

public class Projectile {
	public bool isDestroyed;
	public ProjectileView view;
	public ProjectileModel model;

	public Position origin;
	public Position position;
	public Position target;
	public Player player;
	public int damage;

	public virtual void Tick() {
		view.Tick();
	}

	public virtual void Die() {
		view.Die();
		isDestroyed = true;
	}
}

public class Command {

	static public Dictionary<int, System.Func<Udp.MessageBuffer, Command>> constructors = new Dictionary<int, System.Func<Udp.MessageBuffer, Command>>() {
		{ LineCommand.TypeCode,  (r) => new LineCommand(r) },
		{ TapCommand.TypeCode,  (r) => new TapCommand(r) },
		{ SelectSquadCommand.TypeCode,  (r) => new SelectSquadCommand(r) }
	};

	public virtual void Serialize(Udp.MessageBuffer writer) {
	}

	public virtual void Execute(Player p) {
	}
}

public class LineCommand : Command {
	public const int TypeCode = 1;

	public List<Position> line = new List<Position>();

	public LineCommand() {}

	public LineCommand(Udp.MessageBuffer reader) {
		int count = reader.ReadInt32();
		for (int i = 0; i < count; ++i) {
			float x = reader.ReadSingle();
			float y = reader.ReadSingle();
			line.Add(new Position(x, y));
		}
	}

	override public void Serialize(Udp.MessageBuffer writer) {
		writer.Write((byte)TypeCode);
		writer.Write(line.Count);
		for (int i = 0; i < line.Count; ++i) {
			writer.Write(line[i].x);
			writer.Write(line[i].y);
		}
	}

	override public void Execute(Player p) {
		p.selectedSquad.SetLine(line);
		p.selectedSquad.OrderMove();
	}
}

public class TapCommand : Command {
	public const int TypeCode = 2;

	public Position position;

	public TapCommand() {}

	public TapCommand(Udp.MessageBuffer reader) {
		position.x = reader.ReadSingle();
		position.y = reader.ReadSingle();
	}

	override public void Serialize(Udp.MessageBuffer writer) {
		writer.Write((byte)TypeCode);
		writer.Write(position.x);
		writer.Write(position.y);
	}

	override public void Execute(Player p) {
		p.selectedSquad.OrderPrimaryAction(position);
	}
}

public class SelectSquadCommand : Command {
	public const int TypeCode = 3;

	public byte squadId;

	public SelectSquadCommand() {}

	public SelectSquadCommand(Udp.MessageBuffer reader) {
		squadId = reader.ReadByte();
	}

	override public void Serialize(Udp.MessageBuffer writer) {
		writer.Write((byte)TypeCode);
		writer.Write(squadId);
	}

	override public void Execute(Player p) {
		if (squadId < p.squads.Count) {
			p.selectedSquad = p.squads[squadId];
		}
	}
}

public class _ : MonoBehaviour {

	public static _ self;

	public string backend = "127.0.0.1"; 
	public int port = 3004;
	public Model model;
	public Screens screens;
	public Chui.Root chuiRoot;
	public GameObject moveMarkerPrefab;

	public GameObjectPool pool;
	Udp udp;

	public string login = "Red";

	public GameState gameState = GameState.Login;
	public float lastTickTime = 0;
	public int currentTick = 0;

	public GameObject lobbyScreen;
	public GameObject debugScreen;

	public Player owner;
	public List<Player> players = new List<Player>();
	public List<Unit> units = new List<Unit>();
	public List<Projectile> projectiles = new List<Projectile>();

    private float turnTime;
    public float turnDuration;
	public float lagTime;

    private uint turnReady;
    private uint turnCurrent;

	Server server;
	bool isServer;
	bool isConnected;
	Udp.Remote serverRemote;
	public List<System.Net.IPAddress> availableServers = new List<System.Net.IPAddress>();

	public List<Command> ownerCommands = new List<Command>();

    public bool IsConnected { get { return isConnected; } }
	public bool IsServer { get { return isServer; } }
	public bool IsClient { get { return !isServer; } }

	public int CurrentTurn {
		get {
			return (int)turnCurrent;
		}
	}

	void Awake() {
		self = this;
		Application.targetFrameRate = 60;
		pool = new GameObjectPool();

		udp = new Udp();
		udp.AllowAddressReuse();
		udp.Listen(port+1);

		OpenScreen(_.self.screens.loginScreen);
		debugScreen = OpenScreen(_.self.screens.debugScreen);
	}

	public GameObject OpenScreen(GameObject screenPrefab) {
		Debug.Log("Opening screen " + screenPrefab.name);
		GameObject screen = pool.Pick(screenPrefab);
		_.self.chuiRoot.AddWindow(screen);
		return screen;
	}

	public void CloseScreen(GameObject screen) {
		Debug.Log("Closing screen " + screen.name);
		_.self.chuiRoot.RemoveWindow(screen);
		Object.Destroy(screen);
	}

	public Player GetPlayer(string login) {
		foreach (var p in players) {
			if (p.login == login) {
				return p;
			}
		}
		return null;
	}

	public Player GetPlayer(byte id) {
		foreach (var p in players) {
			if (p.id == id) {
				return p;
			}
		}
		return null;
	}

	void OnMessageStart(Udp.MessageBuffer b) {
		ReadPlayers(b);
		owner = GetPlayer(login);

		foreach (var p in players) {
			p.isReady = true;
		}

		InitBattle();

		gameState = GameState.Battle;
		turnReady = 1;

		CloseScreen(lobbyScreen);
		OpenScreen(screens.gameScreen);
	}

	void OnMessageTurn(Udp.MessageBuffer r) {
		debugScreen.GetComponent<DebugScreen>().AddLag(lagTime);

		turnReady++;
		byte numActivePlayers = r.ReadByte();
		for (int i = 0; i < numActivePlayers; ++i) {
			byte id = r.ReadByte();
			Player p = GetPlayer(id);
			int count = (int)r.ReadByte();
			for (int j = 0; j < count; ++j) {
				byte typeCode = r.ReadByte();
				Command c = Command.constructors[typeCode](r);
				c.Execute(p);
			}
		}
	}

	void Update() {
		udp.Update();

		if (IsServer) {
			server.Update();
		}

		if (IsConnected) {
			UpdateMessages();
		} else {
			UpdateServers();
		}

		switch (gameState) {
			case GameState.Login: UpdateLogin(); break;
			case GameState.Lobby: UpdateLobby(); break;
			case GameState.Battle: UpdateBattle(); break;
			case GameState.Summary: UpdateSummary(); break;
		}
	}

	void UpdateMessages() {
		while (serverRemote.CanRead) {
			var b = serverRemote.BeginRead();
			MessageType type = (MessageType)b.ReadByte();
			
			if (type == MessageType.RoomState) {
				ReadPlayers(b);
			}

			if (type == MessageType.Start) {
				OnMessageStart(b);
			}

			if (type == MessageType.Turn) {
				OnMessageTurn(b);
			}

			serverRemote.EndRead(b);
		}
	}

	void UpdateServers() {
		foreach (var r in udp.remotes) {
			while (r.CanRead) {
				var b = r.BeginRead();
				MessageType type = (MessageType)b.ReadByte();
				if (type == MessageType.RoomState) {
					bool newAddress = true;
					foreach (var a in availableServers) {
						if (a.Equals(r.IPEndPoint.Address)) {
							newAddress = false;
							break;
						}
					}
					if (newAddress) {
						availableServers.Add(r.IPEndPoint.Address);
					}
				}
				if (type == MessageType.End) {
					for (int i = 0; i < availableServers.Count; ++i) {
						if (availableServers[i].Equals(r.IPEndPoint.Address)) {
							availableServers.RemoveAt(i);
							break;
						}
					}
				}
				r.EndRead(b);
			}
		}
	}

	void UpdateLogin() {
	}

	void ReadPlayers(Udp.MessageBuffer b) {
		players.Clear();
		byte numPlayers = b.ReadByte();
		for (int i = 0; i < numPlayers; ++i) {
			var id = b.ReadByte();
			var login = b.ReadString();
			var isReady = b.ReadBoolean();
			Player p = GetPlayer(login);
			if (p == null) {
				p = new Player();
				players.Add(p);
			}
			p.login = login;
			p.id = id;
			p.isReady = isReady;
		}
	}

	public void SetupServer() {
		isServer = true;
		server = new Server();

		ConnectToServer("127.0.0.1");
	}

	public void SendToServer(System.Action<Udp.MessageBuffer> fill, bool reliable = true) {
		var b = reliable ? serverRemote.BeginWriteComplex() : serverRemote.BeginWriteSimple();
		fill(b);
		serverRemote.EndWrite(b);
	}

	public void ConnectToServer(string url) {
		ConnectToServer(udp.GetRemote(url, port));
	}

	public void ConnectToServer(System.Net.IPAddress address) {
		ConnectToServer(udp.GetRemote((System.Net.EndPoint)(new System.Net.IPEndPoint(address, port))));
	}

	void ConnectToServer(Udp.Remote remote) {
		serverRemote = remote;
		isConnected = true;
	}

	public void EnterLobby() {
		gameState = GameState.Lobby;

		SendToServer((b) => {
			b.Write((byte)MessageType.Join);
			b.Write(login);
		});
	}

	void UpdateLobby() {
	}

	public void SetReady() {
		GetPlayer(_.self.login).isReady = true;
		SendToServer((b) => {
			b.Write((byte)MessageType.Ready);
		});
		Debug.Log("<color=blue>Start</color>");
	}

	public void InitBattle() {
		foreach (var p in players) {
			if (p.login == "Red" || p.login == "Purple") {
				Squad s = new Squad();
				for (int i = 0; i < 15; ++i) {
					Unit u = CreateUnit(_.self.model.GetUnitModel("Knecht"), p, Vector2.zero);
					s.AddUnit(u);
				}
				s.SetLine(new List<Position>() { new Position(-1.0f, 0.5f), new Position(1.0f, 0.5f) });
				s.ArrangeUnits();
				p.AddSquad(s);

				s = new Squad();
				for (int i = 0; i < 10; ++i) {
					Unit u = CreateUnit(_.self.model.GetUnitModel("MeleeMutant"), p, Vector2.zero);
					s.AddUnit(u);
				}
				s.SetLine(new List<Position>() { new Position(-1.0f, -0.5f), new Position(1.0f, -0.5f) });
				s.ArrangeUnits();
				p.AddSquad(s);

			} else {
				Squad s = new Squad();
				for (int i = 0; i < 10; ++i) {
					Unit u = CreateUnit(_.self.model.GetUnitModel("Knecht"), p, Vector2.zero);
					s.AddUnit(u);
				}
				s.SetLine(new List<Position>() { new Position(-1.0f, 0.5f), new Position(1.0f, 0.5f) });
				s.ArrangeUnits();
				p.AddSquad(s);

				s = new Squad();
				for (int i = 0; i < 15; ++i) {
					Unit u = CreateUnit(_.self.model.GetUnitModel("MeleeMutant"), p, Vector2.zero);
					s.AddUnit(u);
				}
				s.SetLine(new List<Position>() { new Position(-1.0f, 0.5f), new Position(1.0f, 0.5f) });
				s.ArrangeUnits();
				p.AddSquad(s);
			}


			p.selectedSquad = p.squads[0];
		}
	}

	public Unit CreateUnit(UnitModel model, Player player, Position position) {
		Unit u = model.prefab.GetComponent<UnitView>().Construct();
		u.model = model;
		u.position = position;
		u.hp = model.hp;
		u.view = pool.Pick(u.model.prefab).GetComponent<UnitView>();
		u.view.Init(u);

		units.Add(u);
		return u;
	}
	
	public void DestroyUnit(Unit u) {
		u.Die();
		u.isDestroyed = true;
	}


	public void DealDamage(Player dealer, int damage, Unit enemyUnit) {
		if (!enemyUnit.isDestroyed) {
			enemyUnit.hp -= damage;
			if (enemyUnit.hp <= 0) {
				DestroyUnit(enemyUnit);
			}
		}
	}

	public void DealSplashDamage(Player dealer, Position position, float range, int damage) {
		foreach (var u in GetUnitsWithinRange(position, range)) {
			if (u.player != dealer) {
				if (!u.isDestroyed) {
					DealDamage(dealer, damage, u);
				}
			}
		}
	}

	public Projectile CreateProjectile(ProjectileModel model, Player player, Position origin, Position target) {
		Projectile p = model.prefab.GetComponent<ProjectileView>().Construct();
		p.player = player;
		p.model = model;
		p.position = origin;
		p.origin = origin;
		p.target = target;
		p.view = pool.Pick(p.model.prefab).GetComponent<ProjectileView>();
		p.view.Init(p);

		projectiles.Add(p);
		return p;
	}

	public void DestroyProjectile(Projectile p) {
		p.Die();
		p.isDestroyed = true;
	}


	public IEnumerable<Unit> GetUnitsWithinRange(Position position, float range) {
		//TODO Optimize
		foreach (var u in units) {
			if (!u.isDestroyed && Position.Distance(position, u.position) < range) {
				yield return u;
			}
		}
	}

	public Unit GetNearestEnemyUnit(Player player, Position position) {
		//TODO Optimize
		float nd = 0;
		Unit n = null;
		foreach (var u in units) {
			if (u.squad.player != player && !u.isDestroyed) {
				float d = Position.Distance(position, u.position);
				if (n == null || d < nd) {
					n = u;
					nd = d;
				}
			}
		}
		return n;
	}

	void UpdateBattle() {
		if (turnTime < turnDuration) {
			turnTime += Time.deltaTime;
		}

        if (turnTime > turnDuration) {
            if (turnCurrent < turnReady) {
				turnTime -= turnDuration;
				foreach (Unit u in units) {
					if (!u.isDestroyed) {
						u.Tick();
					}
                }
				foreach (Projectile p in projectiles) {
					if (!p.isDestroyed) {
						p.Tick();
					}
                }

				for (int i = units.Count - 1; i >= 0; --i) {
					if (units[i].isDestroyed) {
						units.RemoveAt(i);
					}
				}
				for (int i = projectiles.Count - 1; i >= 0; --i) {
					if (projectiles[i].isDestroyed) {
						projectiles.RemoveAt(i);

					}
				}
                turnCurrent++;
				SendOwnerCommands();
				lagTime = 0;
            } else {
				lagTime += Time.deltaTime;
			}

        }
	}

	void SendOwnerCommands() {
		SendToServer((b) => {
			b.Write((byte)MessageType.Turn);
			b.Write((byte)ownerCommands.Count);
			for (int i = 0; i < ownerCommands.Count; ++i) {
				ownerCommands[i].Serialize(b);
			}
			ownerCommands.Clear();
		});
	}

	void UpdateSummary() {
	}

	void OnApplicationQuit() {
		if (IsServer) {
			server.End();
		} else {
			if (IsConnected) {
				SendToServer((b) => {
					b.Write((byte)MessageType.End);
				});
			}
		}
	}
}
