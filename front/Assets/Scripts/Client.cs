﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Client {

	const int READ_BUFFER_SIZE = 1024;
	TcpClient client;
	byte[] readBuffer = new byte[READ_BUFFER_SIZE];

	public delegate void MessageWriter(BinaryWriter w);
	public delegate void ResponseCallback(BinaryReader r);
	public delegate void MessageHandler(BinaryReader r);

	uint callId = 0;
	Dictionary<uint, ResponseCallback> callbacks = new Dictionary<uint, ResponseCallback>();

	public Dictionary<byte, MessageHandler> messageHandlers = new Dictionary<byte, MessageHandler>();
	
	BinaryWriter writer;

	MemoryStream responseStream;
	BinaryReader headerReader;
	BinaryWriter responseWriter;

	public bool IsConnected {
		get {
			return client.Connected;
		}
	}

	public void Connect () {
		try {
			string host = _.self.backend;
			int hostPort = _.self.port;
			int crossdomainPort = _.self.port + 1;

			client = new TcpClient(host, hostPort);

			if (client != null) {
				Debug.Log("<color=green>Connection Succeeded</color>");
			} else {
				Debug.Log("<color=red>Connection Failed</color>");
			}

			client.NoDelay = true;
			client.SendBufferSize = 128;

			NetworkStream netStream = client.GetStream();

			BeginNewResponseStream();

			writer = new BinaryWriter(netStream);
		} 
		catch(Exception ex) {
			Debug.Log("<color=red>Server is not active.  Please start server and try again.</color>\n" + ex.ToString());
		}
	}

	public void Disconnect() {
		client.Close();
	}

	void BeginNewResponseStream() {
		responseStream = new MemoryStream();
		responseWriter = new BinaryWriter(responseStream);
		headerReader = new BinaryReader(responseStream);
	}

	void FinishResponse() {
		var reader = new BinaryReader(responseStream);
		ProcessResponse(reader);
		BeginNewResponseStream();
		responseWriter.Write(reader.ReadBytes((int)(reader.BaseStream.Length - reader.BaseStream.Position)));
	}

	void ProcessResponse(BinaryReader reader) {
		byte type = reader.ReadByte();
		if (messageHandlers.ContainsKey(type)) {
			messageHandlers[type](reader);
		} else {
			uint callId = reader.ReadUInt32();
			if (callbacks.ContainsKey(callId)) {
				callbacks[callId](reader);
				callbacks.Remove(callId);
			} else {
				throw new System.Exception("Unhandled message type: " + type);
			}
		}
	}

	public void Update() {
		if (client != null && client.Connected) {
			NetworkStream stream = client.GetStream();

			if (stream.CanRead) {
				while (stream.DataAvailable) {
					int bytesRead = stream.Read(readBuffer, 0, READ_BUFFER_SIZE);
					responseWriter.Write(readBuffer, 0, bytesRead);
				}
			}

			while (responseStream.Length > 5) {
				long oldPosition = responseStream.Position;

				responseStream.Position = 0;
				headerReader = new BinaryReader(responseStream);
				uint length = headerReader.ReadUInt32();
				if (responseStream.Length >= 5 + length) {
					FinishResponse();
				} else {
					responseStream.Position = oldPosition;
					break;
				}
			}
		}
	}

	public void Call(byte type, MessageWriter writeCall = null, ResponseCallback callback = null) {
		uint callId = this.callId++;

		var tempWriter = new BinaryWriter(new MemoryStream());
		if (callback != null) {
			callbacks[callId] = callback;
			tempWriter.Write(callId);
		}
		if (writeCall != null) {
			writeCall(tempWriter);
		}
		tempWriter.Flush();

		writer.Write((uint)tempWriter.BaseStream.Length);
		writer.Write(type);
		writer.Write(((MemoryStream)(tempWriter.BaseStream)).ToArray());

		writer.Flush();
	}
}
