﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoginScreen : MonoBehaviour {

	public GameObject setupServerButton;
	public GameObject loginLabel;
	public GameObject serverLabel;
	public List<GameObject> defaultLoginButtons = new List<GameObject>();
	public GameObject serverButtonPrefab;
	public GameObject serverButtonPack;

	Dictionary<System.Net.IPAddress, GameObject> serverButtons = new Dictionary<System.Net.IPAddress, GameObject>();

	void Awake() {
		Chui.Util.GetEventHandlers(setupServerButton).onClicked.Add((ti) => {

			_.self.SetupServer();
			_.self.EnterLobby();

			_.self.CloseScreen(gameObject);
			_.self.OpenScreen(_.self.screens.lobbyScreen);
			return true;
		});

		foreach (var btn in defaultLoginButtons) {
			var b = btn;
			Chui.Util.GetEventHandlers(b).onClicked.Add((ti) => {
				_.self.login = b.transform.Find("Label").GetComponent<Chui.TextContent>().text;
				return true;
			});
		}
	}
	
	void Update() {
		loginLabel.GetComponent<Chui.TextContent>().text = _.self.login;

		foreach (var a in _.self.availableServers) {
			if (!serverButtons.ContainsKey(a)) {
				var btn = Instantiate(serverButtonPrefab);
				var address = a;
				Chui.Util.GetEventHandlers(btn).onClicked.Add((ti) => {
					_.self.ConnectToServer(address);
					_.self.EnterLobby();
					_.self.CloseScreen(gameObject);
					_.self.OpenScreen(_.self.screens.lobbyScreen);
					return true;
				});
				Chui.Util.GetChildComponent<Chui.TextContent>(btn, "Label").text = address.ToString();
				btn.transform.SetParent(serverButtonPack.transform, false);
				serverButtonPack.GetComponent<Chui.PackLayout>().Revalidate();
				serverButtons[a] = btn;
			}
		}
	}
}
