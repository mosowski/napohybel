﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameScreen : MonoBehaviour {

	public GameObject touchArea;
	public List<GameObject> squadButtons = new List<GameObject>();
	public GameObject lagometr;
	public float stepInterval = 0.1f;
	public float stepMinDistance = 0.2f;
	public float shortTapDuration = 0.1f;

	Chui.TouchInfo touchInfo;
	List<Position> swipe = new List<Position>();
	float lastStepTime = 0;
	float pressTime;

	void Awake() {
		Chui.Util.GetEventHandlers(touchArea).onPressed.Add((ti) => {
			if (touchInfo == null) {
				touchInfo = ti;
				touchInfo.onUpdate += UpdateTouch;
				pressTime = Time.time;
				swipe = new List<Position>();
				swipe.Add(ti.position);
				return true;
			} else {
				return false;
			}
		});

		for (int i = 0; i < squadButtons.Count; ++i) {
			var index = i;
			var b = squadButtons[i];
			Chui.Util.GetEventHandlers(b).onClicked.Add((ti) => {
				_.self.ownerCommands.Add(new SelectSquadCommand() { squadId = (byte)index });
				foreach (var btn in squadButtons) {
					btn.GetComponent<Chui.Widget>().color = Color.white;
				}
				b.GetComponent<Chui.Widget>().color = Color.blue;
				return true;
			});
		}
	}

	void UpdateTouch(Chui.TouchInfo ti) {
		if (ti.phase == Chui.TouchInfo.Phase.End) {
			ti.onUpdate -= UpdateTouch;
			touchInfo = null;
			if (Time.time - pressTime < shortTapDuration) {
				_.self.ownerCommands.Add(new TapCommand() { position = ti.position });
			} else {
				swipe.Add(ti.position);
				_.self.ownerCommands.Add(new LineCommand() { line = swipe });
			}
		} else {
			if (Time.time - lastStepTime > stepInterval || Position.Distance(ti.position, swipe[swipe.Count - 1]) > stepMinDistance) {
				swipe.Add(ti.position);
				lastStepTime = Time.time;
			}
		}
	}

	void Update() {
	
	}
	
}
