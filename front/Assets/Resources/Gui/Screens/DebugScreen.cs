﻿using UnityEngine;
using System.Collections;

public class DebugScreen : MonoBehaviour {
	public Histogram histogram;

	public void AddLag(float lagTime) {
		histogram.Add(lagTime);
	}
}
