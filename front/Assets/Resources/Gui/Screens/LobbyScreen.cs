﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LobbyScreen : MonoBehaviour {

	public GameObject readyButton;
	public GameObject loginLabel;
	public GameObject playersLabel;

	void Awake() {
		_.self.lobbyScreen = gameObject;

		Chui.Util.GetEventHandlers(readyButton).onClicked.Add((ti) => {
			_.self.SetReady();
			readyButton.SetActive(false);
			return true;
		});
	}
	
	void Update() {
		loginLabel.GetComponent<Chui.TextContent>().text = _.self.login;

		string playersList = "";
		foreach (var p in _.self.players) {
			if (p.isReady) {
				playersList += "[color:00ff00]";
			} else {
				playersList += "[color:ff0000]";
			}
			playersList += p.login;
			playersList += "[/color]";
			playersList += "     ";
		}
		playersLabel.GetComponent<Chui.TextContent>().text = playersList;
	}
}
